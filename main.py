#Server File for Blind Travel App

import cv2  #python -m pip install opencv-python
import pymongo  #python -m pip install pymongo

from pydantic import BaseModel  #python -m pip install pydantic
from fastapi.encoders import jsonable_encoder  #python -m pip install  fastapi
from fastapi.responses import JSONResponse
from fastapi import FastAPI, File, UploadFile, HTTPException
import json
#import mongoose as mg
from bson import ObjectId

from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

glob_temp_id = "null"

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

dbClient = pymongo.MongoClient("mongodb://localhost:27017")
Database = dbClient['BlindTravelApp']
print("DB Variable 1 : ",Database)
Database2 = dbClient['BlindTravelApp']
print("DB Variable 2 : ",Database2)

print("Collections : ",Database.list_collection_names())

Conductor = Database["conductor"]
print("DB Conductor Variable: ",Conductor)
Client =  Database["client"]
print("DB Client Variable: ",Client)

class LoginVals(BaseModel):
    email: str
    passwrd: str
    
class SignupVals(BaseModel):
    name: str
    email: str
    password: str
    mobile: str
    currentLat: str
    currentLong: str
    destination: str
    role: str
    picturePath: str

class SetDestVals(BaseModel):
    destination: str
    id: str

class SetLocVals(BaseModel):
    currentLat: str
    currentLon: str
    id: str
    
@app.get("/")
async def Helen():
    print("Test Page Invoked...")
    return "Blind Travel App Server is Up and Running"

@app.post("/login-conductor")
async def login(email: str, passwrd:str):
    result1 = Conductor.find_one({"email":email, "password":passwrd})
    print("Login Request: ")
    print("email:", email)
    print("password: ",passwrd)
    if(result1 is not None):
        print("Login Conductor : ")
        print(result1)
        return result1
    return ""
    
@app.post("/login-client")
async def loginClient(email: str, passwrd:str):
    print("Request Params : ",email,passwrd)
    result2 = Client.find_one({"email":email, "password":passwrd})
    print("Login Request: ")
    print("email:", email)
    print("password: ",passwrd)
    if(result2 is not None):
        print("Login Conductor : ")
        print(result2)   
        print(repr(result2['_id']))
        return str(result2['_id'])
    return ""
    
        
@app.post("/signup")
async def signup(input: SignupVals):
    lat = float(input.currentLat)
    lon = float(input.currentLong)
    record = {
        "name":input.name,
        "email": input.email,
        "password": input.password,
        "mobile": input.mobile,
        "latitude": lat,
        "longitude": lon,
        "destination": input.destination,
        "role": input.role,
        "profilePicturePath":input.picturePath
    }
    #sign up conductor
    Conductor.insert_one(record)
    print("Conductor Signup Succesful")
    return "Conductor has been Succesfully Added..."
        
@app.post("/signup-client")
async def signup(input: SignupVals):
    lat = float(input.currentLat)
    lon = float(input.currentLong)
    record = {
        "name":input.name,
        "email": input.email,
        "password": input.password,
        "mobile": input.mobile,
        "latitude": lat,
        "longitude": lon,
        "destination": input.destination,
        "role": input.role,
        "profilePicturePath":input.picturePath
    }
    #Sign up client
    Client.insert_one(record)
    print("Client Signup Succesful")
    return "Client has been Succesfully Added..."
    
@app.post("/signup-client-new")
async def signup(name:str, email:str, passwrd:str, mob:str, lat:str, lon:str, dest:str, role:str, pic:str):
    record = {
        "name":name,
        "email": email,
        "password": passwrd,
        "mobile": mob,
        "latitude": lat,
        "longitude": lon,
        "destination": dest,
        "role": role,
        "profilePicturePath":pic
    }
    #Sign up client
    Client.insert_one(record)
    print("Client Signup Succesful")
    return "Client has been Succesfully Added..."
    
@app.post("/signup-conductor-new")
async def signup(name:str, email:str, passwrd:str, mob:str, lat:str, lon:str, dest:str, role:str, pic:str):
    print("Conductor Signup started")
    record = {
        "name":name,
        "email": email,
        "password": passwrd,
        "mobile": mob,
        "latitude": lat,
        "longitude": lon,
        "destination": dest,
        "role": role,
        "profilePicturePath":pic
    }
    #Sign up client
    Conductor.insert_one(record)
    print("Conductor Signup Succesful")
    return "Conductor has been Succesfully Added..."
    

@app.get("/get-conductor")
async def getConductor(email: str):
    result = Conductor.find_one({"email":email})
    print("Fetched Conductor : ",result)
    return JSONEncoder().encode({"result":result})

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

@app.get("/get-conductors")
async def getConductors(dest: str):
    print("Requested Destination : ",dest)
    result = Conductor.find({"destination":dest})
    print("Fetched Conductors : ")
    finArr = []
    for x in result:
        finArr.append(x)
        print(x)
    #print(json.dumps(finArr))
    return JSONEncoder().encode({"result":finArr})
    #return {"result":finArr}

@app.get("/get-client")
async def getClient(email: str):
    print("Recieved Email: ",email)
    result = Client.find_one({"email":email})
    print("Fetched Client : ",result)
    return JSONEncoder().encode({"result":result})

@app.get("/get-clients")
async def getClients(dest: str):
    result = Client.find({"destination":dest})
    print("Fetched Clients : ")
    print(result)
    return result

@app.post("/set-dest-conductor")
async def setDestConductor(input: SetDestVals):
    result = Conductor.update_one({"_id":input.id},{"$set":{"destination": "{input.destination}"}})
    print("Updated Conductor Destination :")
    print(result)
    return result

@app.post("/set-dest-client")
async def setDestClient(input: SetDestVals):
    result = Client.update_one({"_id":input.id},{"$set":{"destination": "{input.destination}"}})
    print("Updated Client Destination :")
    print(result)
    return result

@app.post("/update-current-loc-client")
async def updateCurrLocClient(inputId:str, latitude:str, longitude:str):
    lat = float(latitude)
    lon = float(longitude)
    
    result = Client.update_one({"_id":inputId},{"$set":{"latitude": "{lat}", "longitude": "{lon}",}})
    print("Updated Client Current Location :")
    print(result)
    return "Updated Client Location."

@app.post("/update-current-loc-conductor")
async def updateCurrLocConductor(inputId:str, latitude:str, longitude:str):
    lat = float(latitude)
    lon = float(longitude)
    
    result = Conductor.update_one({"_id":inputId},{"$set":{"latitude": "{lat}", "longitude": "{lon}",}})
    print("Updated Conductor Current Location :")
    print(result)
    return "Updated Conductor Location."

@app.post("/update-client")
async def updateClient(input: SignupVals, id: str):
    lat = float(input.currentLat)
    lon = float(input.currentLong)
    record = {
        "name":input.name,
        "email": input.email,
        "password": input.password,
        "mobile": input.mobile,
        "latitude": lat,
        "longitude": lon,
        "destination": input.destination,
        "role": input.role,
    }
    result = Client.update_one({"_id":id},{"$set":{"destination":""}})
    print("Updated Client :")
    print(result)
    return result

@app.post("/update-conductor")
async def updateConductor(input: SignupVals,id: str):
    lat = float(input.currentLat)
    lon = float(input.currentLong)
    record = {
        "name":input.name,
        "email": input.email,
        "password": input.password,
        "mobile": input.mobile,
        "latitude": lat,
        "longitude": lon,
        "destination": input.destination,
        "role": input.role,
    }
    result = Conductor.update_one({"_id":id},{"$set":record})
    print("Updated Conductor :")
    print(result)
    return result

@app.post("/delete-client")
async def deleteClient(id: str):
    result = Client.delete_one({"_id":id})
    print("Deleted Client : ")
    print(result)
    return result

@app.post("/delete-conductor")
async def deleteConductor(id: str):
    result = Conductor.delete_one({"_id":id})
    print("Deleted Conductor : ")
    print(result)
    return result

@app.post("/remove-dest-conductor")
async def removeDestConductor(id: str):
    result = Conductor.update_one({"_id":id},{"$set":{"destination":""}})
    print("Removed Conductor Destination :")
    print(result)
    return result

@app.post("/remove-dest-client")
async def removeDestClient(id: str):
    result = Client.update_one({"_id":id},{"$set":{"destination":""}})
    print("Removed Client Destination :")
    print(result)
    return result

@app.post("/upload-client-picture")
async def uploadClientPic(id:str,image: UploadFile = File(..., description="Upload an image file")):
    #Save image and get path
    Client.update_one({"_id":id},{"$set":{"profilePicturePath":path}})

@app.post("/upload-conductor-picture")
async def uploadConductorPic(image: UploadFile = File(..., description="Upload an image file")):
    #save image and get path
    Conductor.update_one({"_id":id},{"$set":{"profilePicturePath":path}})